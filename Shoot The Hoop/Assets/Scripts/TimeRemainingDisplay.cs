﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeRemainingDisplay : MonoBehaviour {

    LevelManager levelManager;
    Text timeRemainingText;

	// Use this for initialization
	void Start () {
        timeRemainingText = GetComponent<Text>();
        levelManager = FindObjectOfType<LevelManager>();

    }
	
	// Update is called once per frame
	void FixedUpdate () {
        timeRemainingText.text = "Time Remaining: " + levelManager.timeTillNextLevel.ToString("F1");
	}
}
