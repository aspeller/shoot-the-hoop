﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallLauncher : MonoBehaviour {

    public GameObject ballPrefab;
    public float speed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Fire1"))
        {
            // create new ball
            GameObject instance = Instantiate(ballPrefab);

            // make the position of the new ball the same as the camera
            instance.transform.position = transform.position;

            // get the rigidbody
            Rigidbody rb = instance.GetComponent<Rigidbody>();

            // get the camera
            Camera camera = GetComponent<Camera>();

            // set the velocity vector equal to that of the camera rotation, vector forward and the speed
            rb.velocity = camera.transform.rotation * Vector3.forward *  speed;
        }
    }
}
