﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotation : MonoBehaviour
{
    Transform body;
    Transform head;

    // Use this for initialization
    void Start()
    {
        head = GetComponent<Transform>();
        // Debug.Log("Head: " + head.name);

        body = GetComponentInChildren<Camera>().GetComponent<Transform>();
        // Debug.Log("Body: " + body.name);

    }

    // Update is called once per frame
    void Update()
    {
        float rotationSpeed = 5.0f;

        float mouseX = Input.GetAxis("Mouse X") * rotationSpeed;
        float mouseY = Input.GetAxis("Mouse Y") * rotationSpeed;

        head.localRotation *= Quaternion.Euler(0, mouseX, 0);

        body.localRotation *= Quaternion.Euler(-mouseY, 0, 0);
    }
}
